/*
 * This file is part of COMP332 Assignment 1.
 *
 * Copyright (C) 2018 Dominic Verity, Macquarie University.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Tests of the B-tree implementation. Uses the ScalaTest `FlatSpec` style
 * for writing tests. See
 *
 *      http://www.scalatest.org/user_guide
 *
 * For more info on writing ScalaTest tests.
 */

import org.scalatest.FlatSpec
import org.scalatest.Matchers

  import PathPlanner._

class BTreeSpec extends FlatSpec with Matchers {

  import BTree._

  "An empty B-tree:" should "have size 0" in {
    assert (BTree[Int]().size == 0)
  }

  it should "have depth 0" in {
    assertResult(0) {
      BTree[Int]().depth
    }
  }

  it should "return `None` when `find` is invoked" in {
    assertResult(None) {
      BTree[String]().find("Hello")
    }
  }

  it should "return the empty B-tree when `delete` is invoked" in {
    assertResult(EmptyNode[Int]()) {
      BTree[Int]().delete(10)
    }
  }

  it should "return a singleton B-tree when `insert` is invoked" in {
    assertResult(TwoNode(EmptyNode[String](), "Hello", EmptyNode[String]())) {
      BTree[String]().insert("Hello")
    }
  }

  it should "return the empty list when `inOrder` is invoked" in {
    assertResult(Nil) {
      BTree[String]().inOrder
    }
  }

  it should "pretty print to the string \"EmptyNode()\"" in {
    assertResult("EmptyNode()") {
      BTree[(Int, Int)]().toString
    }
  }

  it should "return no node as it is not in the tree" in {
    assertResult(None) {
      val w1 : List[Loc] = (5 to 20).map((_,5)).toList
      val w2 : List[Loc] = (5 to 20).map((_,20)).toList
      val w3 : List[Loc] = (6 to 19).map((20,_)).toList
      val w4 : List[Loc] = (6 to 19).map((5,_)).toList
      val t1 : BTree[Loc] =  BTree(w1 ++ w2 ++ w3 ++ w4)

      t1.find(10,10)
    }

  }

    it should "return 5,20 it is it in the tree" in {
      assertResult(Some(5,20)) {
        val w1 : List[Loc] = (5 to 20).map((_,5)).toList
        val w2 : List[Loc] = (5 to 20).map((_,20)).toList
        val w3 : List[Loc] = (6 to 19).map((20,_)).toList
        val w4 : List[Loc] = (6 to 19).map((5,_)).toList
        val t1 : BTree[Loc] =  BTree(w1 ++ w2 ++ w3 ++ w4)

        t1.find(5,20)
      }
  }

}
