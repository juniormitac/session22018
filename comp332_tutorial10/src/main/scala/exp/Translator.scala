/**
 * Expression language to Stack Machine translator.
 *
 * Copyright 2014, Anthony Sloane, Macquarie University, All rights reserved.
 */

package exp

/**
 * Translator from Expression language source programs to Stack Machine target programs.
 */
object Translator {

    import ExpTree._
    import StackTree._
    import scala.collection.mutable.ListBuffer

    /**
     * Return a Stack Machine program that implements the given Expression language
     * program which came from the given source file.
     */
    def translate (sourcetree : ExpProgram) : StackProgram = {

        // An instruction buffer for translating statements and expressions into
        val instrBuffer = new ListBuffer[StackInstr] ()

        /**
         * Generate an instruction by appending it to the instruction buffer.
         */
        def gen (instr : StackInstr) {
            instrBuffer.append (instr)
        }

        /**
         * The number of labels that have been allcoated so far.
         */
        var labelCount = 0

        /**
         * Allocate a new unique label and return it. The labels will comprise
         * the string "L" followed by a unique count.
         */
        def makeLabel () : String = {
            labelCount = labelCount + 1
            s"L$labelCount"
        }

        /**
         * Translate an expression language program by translating each of its
         * statements and returning the sequence of resulting instructions.
         */
        def translateProgram (program : ExpProgram) : StackProgram = {
            instrBuffer.clear ()
            program.stmts.map (translateStmt)
            gen (Halt ())
            instrBuffer.result ()
        }

        /**
         * Append the translation of a statement to the instruction buffer.
         */
        def translateStmt (stmt : Statement) {
            stmt match {

                case ConstDecl (IdnDef (i), _, exp) =>
                    gen (LValue (i))
                    translateExp (exp)
                    gen (Assign ())

                case ExpStmt (exp) =>
                    translateExp (exp)
                    gen (Print ())

                case IfStmt (cond, stmts) =>
                    val lab = makeLabel ()
                    translateCond (cond, false, lab)
                    stmts.map (translateStmt)
                    gen (Label (lab))

                case SetStmt (IdnExp (IdnUse (i)), exp) =>
                    gen (LValue (i))
                    translateExp (exp)
                    gen (Assign ())

                case WhileStmt (cond, stmts) =>
                    val lab1 = makeLabel ()
                    val lab2 = makeLabel ()
                    gen (Label (lab1))
                    translateCond (cond, false, lab2)
                    stmts.map (translateStmt)
                    gen (Goto (lab1))
                    gen (Label (lab2))

                case _ : VarDecl =>
                    // No code generated for variable declarations

            }
        }

        /**
         * Append the translation of a condition to the instruction buffer
         * followed by a conditional jump based on the value of the condition.
         * The label is the place to which to jump if the condition holds.
         * If jumpIfTrue is true, then the jump is taken if the condition
         * is true, otherwise it is taken if the condition is false. If
         * the jump is not taken, control just continues with the next
         * instruction.
         */
        def translateCond (cond : Expression, jumpIfTrue : Boolean, label : String) {
            translateExp (cond)
            if (jumpIfTrue)
                gen (GoTrue (label))
            else
                gen (GoFalse (label))
        }

        /**
         * Append the translation of an expression to the instruction buffer.
         */
        def translateExp (exp : Expression) {
            exp match {

                case IdnExp (IdnUse (i)) =>
                    gen (RValue (i))

                case IntExp (i) =>
                    gen (Push (i))

                case MinusExp (left, right) =>
                    translateExp (left)
                    translateExp (right)
                    gen (Sub ())

                case PlusExp (left, right) =>
                    translateExp (left)
                    translateExp (right)
                    gen (Add ())

                case SlashExp (left, right) =>
                    translateExp (left)
                    translateExp (right)
                    gen (Div ())

                case StarExp (left, right) =>
                    translateExp (left)
                    translateExp (right)
                    gen (Mul ())

                case _ =>
                    // Other expressions are not translated

            }
        }

        // Translate the program and return the resulting code
        translateProgram (sourcetree)

    }

}
