ThisBuild/organization := "comp.mq.edu.au"
ThisBuild/scalaVersion := "2.12.7"
ThisBuild/scalacOptions :=
    Seq(
        "-deprecation",
        "-feature",
        "-unchecked",
        "-Xcheckinit",
        // "-Xfatal-warnings",
        "-Xlint:-stars-align,_"
    )

// Settings for the assignment 2 project.
lazy val comp332week11 = (project in file("."))
  .settings(
    // Project information
    name := "comp332-week11",
    version := "0.2",

    // Dependencies
    libraryDependencies ++=
      Seq (
        "org.bitbucket.inkytonik.kiama" %% "kiama" %
          "2.2.0" withSources() withJavadoc(),
        "org.bitbucket.inkytonik.kiama" %% "kiama-extras" % "2.2.0" %
          "test" classifier ("tests"),
        "junit" % "junit" % "4.12" % "test",
        "org.scalacheck" %% "scalacheck" % "1.14.0" % "test",
        "org.scalatest" %% "scalatest" % "3.0.5" % "test"
      )
  )

// Interactive settings

logLevel := Level.Info

shellPrompt := {
    state =>
        Project.extract(state).currentRef.project + " " + version.value +
            " " + scalaVersion.value + "> "
}
