//name: Hao PAN Student ID: 43936008

//Jorge Mitac 42877520
package student;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.Map.Entry;

import org.omg.Messaging.SyncScopeHelper;

/*
 * Other classes may appear here
 * 
 */

class Variable{

	
	private String name;
	
	//Jorge , in my method i stored the live lines as a string instead of an array , i find that storing 
	//as a string is easier as you are not moving as many bytes around.
	private String liveLines;
	
	private int register = -1;
	
	
	public Variable (String name , String liveLines ) {
		this.name = name;
		this.liveLines = liveLines;
	}

	public Variable(String name) {
		this.name = name;
	}
	
	public void setliveLines ( String a) {
		this.liveLines = a;
	}


	public String getName() {
		return name;
	}

	public void setRegister(int r) {
		this.register = r;
	}

	public int getRegister() {
		return register;
	}
	
	public String getLines() {
		return liveLines;
	}

}



public class Liveness {

	private final String[] strs = {"live-in","+","-","*","/","mem","[","]"};
	private ArrayList<Variable> variableList = new ArrayList<Variable>();
	private ArrayList<String> tempVars = new ArrayList<String>();
	private int regNum = 1;

	
	private ArrayList<String> lines = new ArrayList<String>();
	
	
	
	private void processInputFile(String fInName) {
		
				
		//scan lines and create an arraylist in the form of variable := variable
		try {
			Scanner in = new Scanner(new File(fInName));
			
			while(in.hasNextLine()) {
				String line = in.nextLine();
				// clean other characters
				for(int i = 0; i < strs.length; i++) {
					line = line.replace(strs[i], " ");
					//line = line.replaceAll("mem\\[.+\\]", " ");
					line = line.replaceAll("\\s+[^a-zA-Z:]+|:|=", " ");
					line = line.replace("live-out", " ");
					line = line.trim();
				}
				lines.add(line);
			}

		} catch (FileNotFoundException e) {
			
		}
		
		
		//creates a line by line analysis of variable state
		
		
		 



		getVariables(lines);
		
		getLiveLines (variableList);

	
	

	}
	
	

	public int getNumRegisters() {
		return regNum -1;
	}


	//compares 2 liveness strings and returns true if they have a line where they are both live

	private static boolean checkStrings(String a , String b) {
		
		String[] partsA = a.split(" ");
		
		String[] partsB = b.split(" ");
		
/*		for (int i = 0 ; i < parts.length ; i++) {
			
			System.out.println(parts[i]);
		}*/
		
		
		for (int i = 0 ; i < partsA.length ; i ++) {
			
			if (partsA[i] == " ") {
				
			
				continue;
			}
			
		for (int j = 0 ; j < partsB.length ; j ++) {
			
			if (partsA[i].equals(partsB[j]) && partsA != partsB) {
				
			
			
				return true;
				
			}
		}
		
	
		
	}
		
	 return false;
		
	}
	
	// returns variables and only variables from a "clean" arraylist
	private void getVariables ( ArrayList <String> a) {

		ArrayList<String> output = new ArrayList<String>();
		
		
		for (int i = 0 ; i < a.size() ; i++) {
			
			for (String word : a.get(i).split(" ")) {
				
				if (!output.contains(word)) {
					output.add(word);
				}
			}
			
		}
		
		for (int i = 0 ; i < output.size() ; i ++) {
			
			Variable myvar = new Variable(output.get(i));
			
			variableList.add(myvar);
			
		}
		
	}
	
	private void getLiveLines ( ArrayList <Variable> variables) {
		
		
		
		for (int i = 0; i < variables.size(); i++) {

			// keeps track of when a value is live
			boolean isLive = false;

			// keeps track of lines where variable is live
			String liveLines = "";

			for (int z = lines.size() - 1; z >= 0; z--) {

				if (z == lines.size() - 1 && lines.get(z).contains(variables.get(i).getName())) {

					isLive = true;
					liveLines = liveLines + Integer.toString(z + 1) + " ";
					continue;
				}

				if (lines.get(z).contains(variables.get(i).getName())) {

					isLive = true;
				}

				if (lines.get(z).substring(0, 2).contains(variables.get(i).getName())) {

					liveLines = liveLines + Integer.toString(z + 1) + " ";
					isLive = false;

				}

				if (isLive == true) {
					liveLines = liveLines + Integer.toString(z + 1) + " ";
				}

			}

			
			
			//store live lines in a mapped tree
			variables.get(i).setliveLines(liveLines);


		}

	}
	


	public TreeMap<String, Integer> generateSolution(String fInName) {
		
		
		// PRE: fInName is a valid input file
		// POST: returns a TreeMap mapping variables (String) to registers (Integer) 

		
		
		TreeMap<String,Integer> varMap = new TreeMap<String,Integer>();
		
		processInputFile(fInName);
		
		int register = 1;
		
		outer : for (int i = 0 ; i < variableList.size() ; i++) {
			
			
			
			if (variableList.get(i).getRegister() != -1) {

				continue outer;
			}
			

			if (i == 0) {

				variableList.get(i).setRegister(register);

				register++;

			}

			if (variableList.get(i).getRegister() == -1) {
				
				variableList.get(i).setRegister(register);
				
				register ++ ;
				
			}
			
			
			
		inner :	for ( int j = i + 1 ; j < variableList.size() ; j++) {
			
			
			
				
				if (variableList.get(j).getRegister() != -1) {
					
					continue inner;
				}
				
			
				
				
				
				if ( checkStrings (variableList.get(i).getLines(), variableList.get(j).getLines()) == false){
					
					variableList.get(j).setRegister(variableList.get(i).getRegister());
					
					variableList.get(i).setliveLines(variableList.get(i).getLines() + (variableList.get(j).getLines() ));
					
					
					
				}
				
				
			}
			
			
			
			
		}
		
		return varMap;
	}

	public void writeSolutionToFile(TreeMap<String, Integer> t, String solnName) {
		
		
		//Jorge -- Writes solution to file

		try {
			File solFile = new File(solnName);
			if(!solFile.exists()) {
				solFile.createNewFile();
			}

			PrintWriter pw = new PrintWriter(solnName);

			int max = 1;
			for(Variable a : variableList) {
				if(max < a.getRegister()) {
					max = a.getRegister();
				}
			}

			pw.println(max);

			String str = "";
			for(Variable variable: variableList) {
				str += variable.getName()+" "+variable.getRegister()+"\n";
			}
			pw.print(str.trim());

			pw.close();

		} catch (IOException e) {
			//System.out.println("something went wrong");
		}
	}


	public static void main(String[] args)
	{

	}

}
