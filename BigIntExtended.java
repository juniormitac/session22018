/* Template for class BigIntExtended.
 * This class is intended to extend the functionality provided 
 * by class BigInt.
 * This template is provided as part of Assignment 2 for COMP333.
 * Every JUnit test routine used in marking each method you write
 * will conform to the stated precondition and description of
 * output for the method.
 */
 package comp333;

import java.math.BigInteger;
import java.util.Random;
import java.util.Scanner;

import javax.sound.midi.Synthesizer;

public class BigIntExtended {
	
	// Euclid's algorithm, extended form.
	// Pre: a and b are positive big integers.
	// Returns: triple (d, x, y) such that
	//          d = gcd(a,b) = ax + by.
	
	//Modified to work with integer only
	
	public static int[] egcd(int a, int b) {
	
		int[] result = new int[3];
		
		int x = 0;
		int y = 1;
		
		int x1 = 1;
		int y1 = 0;
		
		//keeps values of a and b since they are changed in operation below
		int a1 = a;
		int b1 = b;
		
		int temp;
		
		while ( b != 0) {
			
			int q = a/b;
			int r = a % b;
			
			a = b;
			b = r;
			
			temp = x ;
			x = x1 - q * x;
			x1 = temp;
			
			temp = y;
			y = y1 - q * y;
			y1 = temp;
			
		}
		
		int gcd = (a1 * x1) + (b1 * y1);
		
		result[0]= gcd;
		result[1]= x1;
		result[2] = y1;
		
		
		return result;
	}
	
	// Modular inversion.
	// Pre: a and n are positive big integers which are coprime.
	// Returns: the inverse of a modulo n.
	//modified to work with int
	//returns -1 if a and n are not coprime
	public static int minv(Integer a, Integer n) {
		
		Integer result = 0;
		
		int[] prime = egcd (a,n);
		
		//return -1 if greatest common denom is > 1 since numbers are then not coprime
		if (prime[0] != 1) {
			return -1;
		}
		
		else {
			result = ((prime[1] % n) + n ) %n ;
		}
				
		return result;
	}
	
	// Chinese remainder algorithm.
	// Pre: p and q are different big prime numbers.
	//      0 <= a < p and 0 <= b < q.
	// Returns: the unique big int c such that
	//          c is congruent to a modulo p,
	//          c is congruent to b modulo q,
	//          and 0 <= c < pq.
	public static int cra(Integer p, Integer q, Integer a, Integer b) {
		int c ;
		
		int[] primes = egcd(p,q);
		
		
		//returns -1 if p q are not coprime
		if (primes[0] != 1) {
			
			return -1;
			
		}
		
		else {
			c = (a * primes[2] * q) + ( b * primes[1] * p);
		}
			
		
		return c;
	}
	
	// Modular exponentiation.
	// Pre: a and b are nonnegative big integers.
	//      n is a positive big integer.
	// Returns: this method returns a^b mod n.
	//works!
	
	public static BigInt modexp(BigInt a, BigInt b, BigInt n) {

		BigInt y = new BigInt(1);
		BigInt z = a.divide(n)[1];
		BigInt i = b;
		int i_size = b.toString().length();

		while (true) {

			i_size = i.toString().length();

			if (i.toString() == "0") {

				break;
			}

			if ((int) i.toString().charAt(i_size - 1) % 2 == 1) {

				y = z.multiply(y).divide(n)[1];

			}

			z = z.multiply(z).divide(n)[1];

			i = i.divide(new BigInt(2))[0];

		}

		return y;
	}

	// Modular exponentiation, version 2.
	// This method returns a^b mod n, in case n is prime.

	public static BigInt modexpv2(BigInt a, BigInt b, BigInt n) {

		BigInt result = new BigInt();
		
		
		//test if n is prime otherwise return 0
		
		if (millerrabin(n , 3) == false ) {
			System.out.println("N is not prime !");
			return new BigInt("0");
		}

		// using fermat little theorem
		// if b > n then precondition not followed
		if (n.lessOrEqual(b) == false) {
			System.out.println("Precondition not met , using modexp");
			return modexp(a, b, n);

		}

		// fermat little theorem states that a is an integer that is not a multiple of p
		if (a.divide(n)[1].isEqual(new BigInt("0"))) {
			System.out.println("a is a multiple of p , therefore fermat can not be used");
			return new BigInt("0");

		}

		
		BigInt rest = b.divide(n.subtract(new BigInt("1")))[1];

		result = modexp(a, rest, n);

		return result;

	}

	// Modular exponentiation, version 3.
	// This method returns a^b mod n, in case n = pq, for different
	// primes p and q.
	public static int modexpv3(int a, int b, int n) {
		
		/*
		 * just a note unless i am given p and q there is no way i can do this with the
		 * chinese remainder theorem . And are we expected to find a mathematically
		 * efficient method of finding the factors of a large prime number? that is one
		 * of the biggest problems of computer science . For a small n i might be able
		 * to but for anything above 2 ^ 32 bits number the method i can think of is too
		 * slow besides using something like pollard rho but we are not told to
		 * implement that on the assignment . i just put down a copy of modexpv2
		 */

		if (a > Integer.MAX_VALUE || b > Integer.MAX_VALUE || n > Integer.MAX_VALUE) {
			
			System.out.println("Use modexp or modexpv2");
			return 0;
		}
		
		
		return Integer.valueOf(
				modexpv2(new BigInt(String.valueOf(a)), new BigInt(String.valueOf(b)), new BigInt(String.valueOf(n)))
						.toString());

	}
		
	// Pre: a and b are nonnegative big integers of equal length.
	// Returns: the product of a and b using karatsuba's algorithm.

	
	
	public static BigInt karatsuba ( BigInt x , BigInt y) {
		
		BigInt result = new BigInt(0);
		
		//Return 0 as precondition not met
		
		if (x.toString().length() != y.toString().length()) {
			System.out.println("Precondition not met !");
			return result;
		}
		
		//if number is small just do normal multiplication
		if ( x.toString().length() <= 10) {
			result = x.multiply(y);
			return result;
		}
	
		
		int size1 = x.toString().length();
		int size2 = y.toString().length();
		
		int N = Math.max(size1, size2);
		
		N  = (N/2) + (N % 2);
		
		BigInt m = new BigInt(String.valueOf((long)Math.pow(10, N)));
		
		BigInt b = x.divide(m)[1];
		
		BigInt a = x.subtract(b.multiply(m));
		
		BigInt d = y.divide(m)[0];
		
		BigInt c = y.subtract(d.multiply(new BigInt(Integer.toString(N))));
		
		
		//Compute subexpression
		
		BigInt z0 = a.multiply(c);
		
		BigInt z1 = a.add(b).multiply(c.add(d));
		
		BigInt z2 = b.multiply(d);
		
		result = z0.add(z1.subtract(z0).subtract(z2).multiply(m)).add(z2.multiply(new BigInt(Long.toString((long)(Math.pow(10, 2 * N))))));
		
		
		return result;
		
	}
	
	
	// Pre: n is an odd big integer greater than 4.
	//      s is an ordinary positive integer.
	// Returns: if n is prime then returns true with certainty,
	//          otherwise returns false with probability
	//          1 - 4^{-s}.
	public static boolean millerrabin(BigInt n, int iteration) {

		BigInt zero = new BigInt();
		BigInt one = new BigInt("1");
		BigInt two = new BigInt("2");

		// precondition not met

		if (n.lessOrEqual(new BigInt(4)) == true || iteration < 0) {

			return false;
		}

		// base case , o and 1 are not primes
		if (n.isEqual(zero) || n.isEqual(one)) {
			return false;
		}

		// 2 is prime

		if (n.isEqual(new BigInt(2))) {
			return true;
		}

		// even numbers are not prime

		if (Integer.parseInt(n.divide(new BigInt(2))[1].toString()) == 0) {

			return false;

		}

		BigInt s = n.subtract(one);

		while (s.divide(two)[1].isEqual(zero)) {

			s = s.divide(two)[0];
		}

		// System.out.println("final s " + s.toString());

		Random rand = new Random();

		for (int i = 0; i < iteration; i++) {

			BigInt r = new BigInt(String.valueOf(Math.abs(rand.nextLong())));
			BigInt a = r.divide(n.subtract(one))[1].add(one);

			BigInt temp = s;

			BigInt mod = modexp(a, temp, n);

			while (temp.isEqual(n.subtract(one)) == false && mod.isEqual(one) == false
					&& mod.isEqual(n.subtract(one)) == false) {

				mod = mod.multiply(mod).divide(n)[1];

				temp = temp.multiply(two);

			}

			if (mod.isEqual(n.subtract(one)) == false && temp.divide(two)[1].isEqual(zero)) {

				return false;
			}
		}

		return true;
	}

	// Pre: l and s are ordinary positive integers.
	// Returns: a random "probable" big prime number n of length l decimal digits.
	// The probability that n is not prime is less than 4^{-s}.
	public static BigInt randomprime(int l, int s) {

		BigInt result = new BigInt();
		String number = "";

		// 0-10 primes for first digit
		int[] primes = { 1, 3, 7, 5, 2 };

		if (l == 1) {

			number = String.valueOf(primes[new Random().nextInt(primes.length)]);
			return new BigInt(number);
		}

		for (int i = 0; i < l; i++) {

			int add = 0;

			if (i == 0) {
				add = new Random().nextInt(primes.length);
				number = String.valueOf(primes[new Random().nextInt(primes.length - 2)]);
				continue;
			}

			add = new Random().nextInt(9) + 1;
			number = String.valueOf(add) + number;

			if (number.length() == l) {

				if (millerrabin(new BigInt(number), s) == true) {

					result = new BigInt(number);

					return result;
				}

				number = "";
				i = -1;

			}

		}

		return result;
	}
	
	
	//simple euclidean algorithm
	public static int gcd ( int a , int b) {
		if (b == 0 ) return a;
			return gcd(b, a% b);
	}


	public static void main(String[] args) {
		
	

		/*
		 * Begin RSA demonstration please remenber that i am using my bigint methods to
		 * generate a random prime however assume that maximun size of a number is the
		 * maximun number of an int size
		 */

		Scanner scanner = new Scanner(System.in);
		String number;

		// Primes

		int p,q = 0;

		// N

		int n = 0;

		// e

		int e = 0;

	
		System.out.println("Please enter a lenght for p and q");
		number = scanner.nextLine();

		
		//Generating Public Key
		p = Integer.valueOf(randomprime(Integer.valueOf(number), 4).toString());
		q = Integer.valueOf(randomprime(Integer.valueOf(number), 4).toString());

		// if primes are equal redo
		while (true) {
			
			if (p == 1 || q == 1) {
				
				p = Integer.valueOf(randomprime(Integer.valueOf(number), 4).toString());
				q = Integer.valueOf(randomprime(Integer.valueOf(number), 4).toString());
				continue;
				
				
			}

			if (p == q) {

				p = Integer.valueOf(randomprime(Integer.valueOf(number), 4).toString());
				q = Integer.valueOf(randomprime(Integer.valueOf(number), 4).toString());
				continue;
			}

			break;

		}
		
		System.out.println(("p and q are " + p + " " + q));
		
		n = p * q;
		
		System.out.println("Your n is " + n);
		
		int theta = (p-1)*(q-1);
		
		System.out.println("Your thetha is " + theta);

		e = Integer.valueOf(randomprime(Integer.valueOf(number), 4).toString());
		
		while (true) {
			
			if (gcd (e, theta) != 1) {
				
				e = Integer.valueOf(randomprime(Integer.valueOf(number), 4).toString());
				continue;
				
			}
			
			break;
		}
	
		

		
		
		System.out.println("Your e is " + e);
		System.out.println("Those 2 make up the public key");
		
		//Generating private key
		
		
		
		int d = ((new Random().nextInt(10) * theta)+ 1) / e;
		
		System.out.println("Your private key is " + d);
		
		
		System.out.println("This is a simple encryption please enter a low number to encrypt ");
		
		int encryption = scanner.nextInt();
		
		encryption = encryption ^ e % n;
		
		System.out.println("This is your encrypted number " + encryption);
		
		//thats it !
		
	}
}
