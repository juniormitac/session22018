//Written in notepad ++ for better code display

Assignment 2 report

This assignment was concerned with the design testing and a report of designing a syntax analyser for the Lintilla programming language. A syntax analyser was implemented and it will be the main focus of this report. 
In this report it will be found details about the design and implementation of the syntax analyser as well as the design of tests to check the correctness of the code. The full source code was submitted along with this 
report.

Parsing

In this section the implementation of the parser will be described . first we will start to describe parsing without consideration for tree building , we will for a later section explain the steps taken to build a 
correct tree.

For parsing scala combinator parsers are used , these include rep, repsep/rep1sep as well as the opt keyword for optional parsing of expressions separated by commas as explained by the design document.

Expressions

The parsers for most expressions are missing from the framework code , we are given however the productions that specify how expressions are parsed , i will go through some of them to explain the reasoning for each 
parser implementation . 

Starting we are given a grammar for lintilla however this grammar is ambiguous and will not produce a correct tree if used , as a first step expressions were divided between which ones whose order in parsing matter 
to other ones that do , from this we get the following grammar 

expression : assign
    | factors
    | ifExp
    | whileExp
    | returnExp
    | letDecl
    | fnDecl
    | block

factors : factors "=" factors1
     | factors "<" factors1
     | factors “:=” factors1
     |factors1

factors1 : factors1 “+” factors2
     |factors2 “-” factors2
     |factors2

factors2 : “true”
     |”false
     |factors2 “*” factors2
     |factors2 “/” factors2
     |Integer
     |”(“ expression “)”
     | “-” factors2
     |block
     |appExp

    
As we can from this the expressions were separated to make tree construction clear and eliminate ambiguity from the language . We have the expression declaration which parses each expression that do not require 
a specific order and that was separated from factors which do . On factors order of operation is necessary for example “*” expression had to be processed before a “+” expression , the expressions factors, 
factors1, factor2 denote the precedence rules one for each precedence level as explained in lectures.

Parser Construction

In this section we will cover the process of creating parsers for each expression, as given to us by the grammar in the design document we can infer what the parser should be like . we are given the following grammar :



app : app "(" ((exp ",")* exp)? ")"
    | idnuse.

assign : idnuse ":=" exp.

block : "{" ((exp ";")* exp)? "}".

ifexp : "if" exp block "else" block.

whileexp : "while" exp block.

returnexp : "return" exp?.

letdecl : "let" "mut"? idndef "=" exp.

fndecl : "fn" idndef "(" ((paramdecl ",")* paramdecl)? ")" ("->" tipe)? block.

paramdecl : idndef ":" tipe.

Since i have already established the precedence rules for all expression it is just a matter of of building an expression parser which contains most parsers that are not operators and adds to the tail the factor parser established above.

lazy val expression : PackratParser [Expression] =
     factors|paramDecl|letDecl|whileExp|assignExp|block|fnDecl|ifExp|returnExp|appExp

As an example of parser construction we can take block expression and go through the steps required to build the parser :

As we see from the requirements we have that -block : "{" ((exp ";")* exp)? "}" which is zero or more expressions separated by commas enclosed by brackets , we can express this simply using the combinator parser repsep and enclosing 
it with ~> <~ finally passing everything as a parameter using ^^ on Block

       lazy val block : PackratParser [Block] =
         "{" ~>repsep(expression , ";")  <~ "}" ^^ Block 

repsep denotes an expression separated by commas the optional expression at the end is expressed by repsep as it denotes zero or more separated by a semicolon.

Another example where i had to use further methods was the expression letDecl denoted by 

letdecl : "let" "mut"? idndef "=" exp.




lazy val letDecl : PackratParser [Expression] =
    ("let" ~> opt("mut")) ~ (idndef <~ "=") ~ expression ^^ {
        case None ~ n ~ e => LetDecl(n,e) // code to build a `LetDecl` node.
        case Some(_) ~ n ~ e => LetMutDecl (n,e)// code to build a `LetMutDecl` node.
    }
 
To construct the parser a similar method was used using combination parsers as well as an option parser, the ~ operator denotes 2 different parameters that are to be passed onto LetDecl. 
In this case however there is an exception for example the requirements require that the parser be able to differentiate between inputs “let” and “let” followed by an optional “mut” to 
accomplish this pattern matching as explained on lectures was used so that expressions with only “let” are parsed as a LetDecl expression and expressions with “let mut” are parsed as a 
LetMutDecl expression.

Using similar techniques we constructed the rest of the parsers

  lazy val paramDecl : PackratParser[ParamDecl] =
     (idndef <~ ":") ~ tipe ^^ ParamDecl

    lazy val assignExp : PackratParser[AssignExp] =
      (idnuse <~ ":=" ) ~ expression ^^ AssignExp

     
   lazy val ifExp : PackratParser [IfExp] =
   ("if" ~> expression ) ~ block ~ ("else" ~> block) ^^ IfExp

    lazy val whileExp : PackratParser [WhileExp] =
      ("while" ~> expression) ~ block ^^ WhileExp

   lazy val returnExp : PackratParser [Return] =
     "return" ~> opt(expression) ^^ {case Some(tp) => Return(Option(tp))
                                    case None  => Return(None)}

   lazy val  fnDecl : PackratParser [FnDecl] =
   "fn" ~> idndef  ~ ("(" ~> (repsep(paramDecl , ",") ) <~ ")") ~ opt("->" ~> tipe) ~ block ^^ FnDecl

 lazy val appExp : PackratParser [Expression] =
     appExp ~ ("(" ~> repsep(expression, ",") <~ ")") ^^ {case a ~ b => AppExp(a,b)
     }|idnuse ^^ IdnExp



Types

As explained by the requirements document we have a set of types which are built in a similar way as the parsers 


    lazy val tipe : PackratParser[Type] =
    "unit" ^^^ UnitType() |
    "bool" ^^^ BoolType() |
    "int" ^^^ IntType()|
       "(" ~> tipe <~")" |
"fn" ~> ("(" ~> repsep(tipe,",") <~ ")") ~ opt("->" ~> tipe) ^^ {
case tps ~ Some(tp)=>FnType(tps,tp)
case tps ~ None     => FnType(tps, UnitType())}|

In this case since we are given the keywords as part of the framework and we are told which are the types that exist in the language we can simply use the ^^^ operator to denote the all the types as seen above .

Hierarchy 

As explained in the first section , hierarchy is used to make sure the tree is built in the correct order to illustrate we can go to the factors, factors1, factors2 expression 

  lazy val factors : PackratParser [Expression] =
    factors ~ ("=" ~> factors1) ^^ EqualExp |
    factors ~ ("<" ~> factors1) ^^ {case e ~ t => LessExp (e,t) } |
    idnuse ~ (":=" ~> factors1) ^^ {case e ~ t => AssignExp(e,t)}|
    factors1


  lazy val factors1 : PackratParser [Expression] =
    factors1 ~ ("+" ~> factors2) ^^ {case e ~ t => PlusExp (e,t) } |
    factors1 ~ ("-" ~> factors2) ^^ {case e ~ t => MinusExp (e,t) } |
    factors2

  lazy val factors2 : PackratParser[Expression] =
    "true" ^^^ BoolExp(true) |
    "false" ^^^ BoolExp(false) |
    factors2 ~ ("*" ~> factors2) ^^ {case e ~ t => StarExp (e,t)}|
    factors2 ~ ("/" ~> factors2) ^^ {case e ~ t => SlashExp (e,t)}|
    integer ^^ (s => IntExp (s.toInt))|
    //idnuse ^^ ( s => IdnExp (s))|
    "(" ~> expression <~ ")"|
    "-" ~> factors2 ^^ {case e => NegExp(e)}|
    block|
    appExp
	
as we can see each expression is executed according to a hierarchyas explained in the requirements , for example when the program encounters a symbol that does not match any of the expressions already made it goes into 
the factors expression what it does is that it will try to match the symbol to one of the expressions and depending on where that symbol is it will parse it , for example a "+" symbol the parser will look into the factors 
expression and if it doesnt find it(which it wont) it will go into factors 1 where it will find it , if it cant find the symbol the parser will complain.
And example of hierarchy is that for exmaple if we have the expression (a + 4 * 5) the parser will find both symbols "+" and "*" but because the symbol "*" is higher in the hierchy (factors2 vs factors1) it will get parsed 
first.


Tree Building

As explained in the parser construction section tree building is done by using the ^^ operator , since that operator will take the parser and pass it as argument on the LintillaTree classes since we have this done 
automatically by the implicit mechanism of Scala we only need to consider special paramaters for edge cases an example of this is the letDecl explained in the section above.

Testing 

The aim of my testing was to cover the obvious combinations of constructs that would allow me to within reason consider my parser be able to create correct trees , it is impossible to test every possible combination 
of inputs however the aim of this testing was to find error in common use cases.
A divide and conquer methodology was used , this means that the problem of building a correct tree is divided into subproblems so that by the idea of a solution being the combination of solutions to sub problems we can 
obtain a solution that it is suited for most cases.

To do this i divided my testing by steps :

-Test the basic expressions make sure that parsers work for use cases
-Test operators such as "*,+,-" to make sure they are recognised 
-Test that precedence of operators work
-Test that parenthesis override precende

Starting with testing parsers for every expression an example are the following tests :

test ("testing block expression with a small + input") {
  expression("{4 + 2}") should parseTo[Expression] {
    Block(Vector(PlusExp(IntExp(4), IntExp(2))))
  }
}

test("parsing an empty block expression should just ahve an empty vector inside") {
  expression("{}") should parseTo[Expression] {
    Block(Vector())
  }
}

test ("testing let declaration") {
  expression ("let a = 1") should parseTo[Expression]{
    LetDecl(IdnDef("a"), IntExp(1))
  }
}

In this example my testing makes sure that parsing works for common input on single expressions, i do this for every expression and using my divide and conquer methodology i can make sure that every expression is working
correctly for example on block i made sure to test cases for where a block has an expression inside and another one for when a block has an expression outside .

I did encountered some problems such as a problem dealing with keywords "let" and "mut" the assignment requiremed that i differentiated between both of them because LetDecl and LetMutDecl are different expressions, on 
my first try "let mut" would be considered as part of the same expression instead of separate.The following test made me realise my error and i corrected it the reasoning of how i fixed is explained in the section above


test ("testing let mut declaration") {
  expression ("let mut a = 1") should parseTo[Expression]{
   LetMutDecl(IdnDef("a"), IntExp(1))))
  }
}




Next i test operation expressions , these are the symbols that perform arythmetic operations on expressions , here are some examples 

test ("test * operator") {
  expression("4 * 2") should parseTo[Expression] (
    StarExp(IntExp(4), IntExp(2))
  )
}

test ("testing / operations") {
  expression("4 / 2") should parseTo[Expression] (
    SlashExp(IntExp(4), IntExp(2))
  )
}


test ("testing negation operator inside integer operations") {
  expression ("4 + (-2)") should parseTo[Expression] {
    PlusExp(IntExp(4), NegExp(IntExp(2)))
  }
}
  
As we can see the tests are very simple following the bottom up approach on testing , to note is the testing on parenthesis on expressions according to point 4 of testing testing.

Finally once we make sure that expression parsers are working properly and operation expressions are working correctly we cna continue onto more complicated testing. In this case more complex expressions are used , as explained 
not all possible inputs are possible to be tested but an attempt is done by using the provided example programs as well as some programs of my own.

An example of this is testing block operations , since blocks are expressions and from factors2 expression on the previous section it should be possible to pass 2 block expressions as parameters in an arithmetic operation 

test ("complex block operation") {
  expression ("{ let x = 5; x + x } * { let y = 10; y / 4 }") should parseTo [Expression] {

    StarExp(
        Block(
            Vector(
                LetDecl(IdnDef("x"), IntExp(5)),
                PlusExp(IdnExp(IdnUse("x")), IdnExp(IdnUse("x"))))),
        Block(
            Vector(
                LetDecl(IdnDef("y"), IntExp(10)),
                SlashExp(IdnExp(IdnUse("y")), IntExp(4)))))
  }
}

the above test produces a correct output tree and thus is an example of the tests i made on expressions to test the correctness of my code.

Until then i was only testging expressions as denoted by expression ("expression") should parseTo [Expression] {} on my tests , at this point i went onto test more complex programs , for this i used the 
  program ("program") should parseTo[Program]{} to test more complex programs an example of this is the program factorial given as an example and its test
  
  test ("factorial function") {
  program ("fn fact(n: int) -> int {if n = 0 { 1 } else { n * fact(n-1) }};fact(5)") should parseTo[Program]{
    Program(
        Vector(
            FnDecl(
                IdnDef("fact"),
                Vector(ParamDecl(IdnDef("n"), IntType())),
                Some(IntType()),
                Block(
                    Vector(
                        IfExp(
                            EqualExp(IdnExp(IdnUse("n")), IntExp(0)),
                            Block(Vector(IntExp(1))),
                            Block(
                                Vector(
                                    StarExp(
                                        IdnExp(IdnUse("n")),
                                        AppExp(
                                            IdnExp(IdnUse("fact")),
                                            Vector(
                                                MinusExp(
                                                    IdnExp(IdnUse("n")),
                                                    IntExp(1))))))))))),
            AppExp(IdnExp(IdnUse("fact")), Vector(IntExp(5)))))
  }
}
As expected my program output was correct since i had already debugged most associativity and keyword problems from earlier testing and since every program is simple a combination of expressions , most bugs would be 
encountered on my expression testing and not my program testing , i decided not to worry much about my program testing and devoted my time on testing my expressions for correctness hence the higher amount of testing done to expressions
rather than programs.

