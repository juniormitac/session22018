/*
 * This file is part of COMP332 Assignment 2 2018.
 *
 * Lintilla, a simple functional programming language.
 *
 * © 2018, Dominic Verity and Anthony Sloane, Macquarie University.
 *         All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Tests of the parser of the Lintilla language.
 */

package lintilla

import org.bitbucket.inkytonik.kiama.util.ParseTests
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

/**
  * Tests that check that the parser works correctly.  I.e., it accepts correct
  * input and produces the appropriate trees, and it rejects illegal input.
  */
@RunWith(classOf[JUnitRunner])
class SyntaxAnalysisTests extends ParseTests {

  import LintillaTree._

  val parsers = new SyntaxAnalysis(positions)
  import parsers._

  // Tests of parsing terminals

  test("parsing an identifier of one letter produces the correct tree") {
    identifier("x") should parseTo[String]("x")
  }

  test("parsing an identifier as an applied instance produces the correct tree") {
    idnuse("count") should parseTo[IdnUse](IdnUse("count"))
  }

  test("parsing an identifier containing digits and underscores produces the correct tree") {
    idndef("x1_2_3") should parseTo[IdnDef](IdnDef("x1_2_3"))
  }

  test("parsing an integer as an identifier gives an error") {
    identifier("42") should
    failParseAt(1, 1,
                "string matching regex '[a-zA-Z][a-zA-Z0-9_]*' expected but '4' found")
  }

  test("parsing a non-identifier as an identifier gives an error (digit)") {
    identifier("4foo") should
    failParseAt(1, 1,
                "string matching regex '[a-zA-Z][a-zA-Z0-9_]*' expected but '4' found")
  }

  test("parsing a non-identifier as an identifier gives an error (underscore)") {
    identifier("_f3") should
    failParseAt(1, 1,
                "string matching regex '[a-zA-Z][a-zA-Z0-9_]*' expected but '_' found")
  }

  test("parsing a keyword as an identifier gives an error") {
    identifier("let ") should failParseAt(1, 1,
                                          "identifier expected but keyword found")
  }

  test("parsing a keyword prefix as an identifier produces the correct tree") {
    identifier("letter") should parseTo[String]("letter")
  }

  test("parsing an integer of one digit as an integer produces the correct tree") {
    integer("8") should parseTo[String]("8")
  }

  test("parsing an integer as an integer produces the correct tree") {
    integer("99") should parseTo[String]("99")
  }

  test("parsing a non-integer as an integer gives an error") {
    integer("total") should
    failParseAt(1, 1,
                "string matching regex '[0-9]+' expected but 't' found")
  }

//Begin Operator Testing

//Integer operations

test ("testing + operator") {
  expression("4 + 2") should parseTo[Expression] (
    PlusExp(IntExp(4), IntExp(2))
  )
}

test("testing - operator") {
  expression("4 - 2") should parseTo [Expression] (
    MinusExp (IntExp(4), IntExp(2))
  )
}

test ("test * operator") {
  expression("4 * 2") should parseTo[Expression] (
    StarExp(IntExp(4), IntExp(2))
  )
}

test ("testing / operations") {
  expression("4 / 2") should parseTo[Expression] (
    SlashExp(IntExp(4), IntExp(2))
  )
}

test ("testing precedence combining + and * parsers") {
  expression("4 + 2/3") should parseTo[Expression] (
    PlusExp(IntExp(4),SlashExp(IntExp(2), IntExp(3)))
  )
}

//testing other operators
//Begin Testing other operators

test ("Testing negation operator") {
  expression ("-2") should parseTo[Expression] (
    NegExp(IntExp(2))
  )
}

test ("testing negation operator inside integer operations") {
  expression ("4 + (-2)") should parseTo[Expression] {
    PlusExp(IntExp(4), NegExp(IntExp(2)))
  }
}

test ("testing less than operator") {
  expression ("a < b") should parseTo[Expression] {
    LessExp(IdnExp(IdnUse("a")), IdnExp(IdnUse("b")))
  }
}

//End testing other operators

//End Operator Testing

//Begin Expression Testing


test ("testing block expression with a small + input") {
  expression("{4 + 2}") should parseTo[Expression] {
    Block(Vector(PlusExp(IntExp(4), IntExp(2))))
  }
}

test("parsing an empty block expression should just ahve an empty vector inside") {
  expression("{}") should parseTo[Expression] {
    Block(Vector())
  }
}

test ("testing let declaration") {
  expression ("let a = 1") should parseTo[Expression]{
    LetDecl(IdnDef("a"), IntExp(1))
  }
}

test ("testing let mut declaration") {
  expression ("let mut a = 1") should parseTo[Expression]{
   LetMutDecl(IdnDef("a"), IntExp(1))
  }
}

test("assigning a variable an integer value") {
  expression("a := 4") should parseTo[Expression]{
    AssignExp(IdnUse("a"),IntExp(4))
  }
}

test ("assigning a variable another variable") {
  expression ("a := b") should parseTo [Expression] {
    AssignExp(IdnUse("a"), IdnExp(IdnUse("b")))
  }
}



test ("testing simple parameter declaration inside a function") {
  expression ("fn f(a : unit) {}") should parseTo [Expression] {
    FnDecl(
             IdnDef("f"),
             Vector(ParamDecl(IdnDef("a"), UnitType())),
             None,
             Block(Vector()))
  }
}


test ("testing a simple while expression") {
  program("while a = b {}") should parseTo[Program] {
    Program(
        Vector(
            WhileExp(
                EqualExp(IdnExp(IdnUse("a")), IdnExp(IdnUse("b"))),
                Block(Vector()))))
  }
}



test ("testing a complex while expression ") {
  expression("{let mut x = 10;while (0 < x) {if x - (x / 2) * 2 = 0 {  return} else {  x := 2 * x - 1  };x := (x + 1) / 2  }  }") should parseTo [Expression]{

    Block(
        Vector(
            LetMutDecl(IdnDef("x"), IntExp(10)),
            WhileExp(
                LessExp(IntExp(0), IdnExp(IdnUse("x"))),
                Block(
                    Vector(
                        IfExp(
                            EqualExp(
                                MinusExp(
                                    IdnExp(IdnUse("x")),
                                    StarExp(
                                        SlashExp(
                                            IdnExp(IdnUse("x")),
                                            IntExp(2)),
                                        IntExp(2))),
                                IntExp(0)),
                            Block(Vector(Return(None))),
                            Block(
                                Vector(
                                    AssignExp(
                                        IdnUse("x"),
                                        MinusExp(
                                            StarExp(
                                                IntExp(2),
                                                IdnExp(IdnUse("x"))),
                                            IntExp(1)))))),
                        AssignExp(
                            IdnUse("x"),
                            SlashExp(
                                PlusExp(IdnExp(IdnUse("x")), IntExp(1)),
                                IntExp(2))))))))

  }
}

test("testing a simple ifExp"){
  expression("if a=b {a=c} else {}") should parseTo[Expression]{

    IfExp(
            EqualExp(IdnExp(IdnUse("a")), IdnExp(IdnUse("b"))),
            Block(
                Vector(EqualExp(IdnExp(IdnUse("a")), IdnExp(IdnUse("c"))))),
            Block(Vector()))
  }
}

test ("testing complex ifExp") {
  expression ("if x = y {if true { 3 } else { 4 }} else {5}") should parseTo [Expression]{
    IfExp(
        EqualExp(IdnExp(IdnUse("x")), IdnExp(IdnUse("y"))),
        Block(
            Vector(
                IfExp(
                    BoolExp(true),
                    Block(Vector(IntExp(3))),
                    Block(Vector(IntExp(4)))))),
        Block(Vector(IntExp(5))))


  }
}

test ("testing empty return expression") {
  expression ("return") should parseTo[Expression]{
    Return(None)
  }
}

test("testing simple return expression") {
  expression("return a") should parseTo[Expression]{
    Return(Some(IdnExp(IdnUse("a"))))
  }
}



test ("complex block operation") {
  expression ("{ let x = 5; x + x } * { let y = 10; y / 4 }") should parseTo [Expression] {

    StarExp(
        Block(
            Vector(
                LetDecl(IdnDef("x"), IntExp(5)),
                PlusExp(IdnExp(IdnUse("x")), IdnExp(IdnUse("x"))))),
        Block(
            Vector(
                LetDecl(IdnDef("y"), IntExp(10)),
                SlashExp(IdnExp(IdnUse("y")), IntExp(4)))))
  }
}

test ("block with function inside") {
  expression ("{fn double(x : int) -> int {  x * 2  };  double(4)};") should parseTo[Expression] {

    Block(
        Vector(
            FnDecl(
                IdnDef("double"),
                Vector(ParamDecl(IdnDef("x"), IntType())),
                Some(IntType()),
                Block(Vector(StarExp(IdnExp(IdnUse("x")), IntExp(2))))),
            AppExp(IdnExp(IdnUse("double")), Vector(IntExp(4)))))

  }
}

test ("function with block ") {
  expression ("fn uncurried(x : int, y : bool) -> int {if y { x } else { x * 2 }  };") should parseTo[Expression]{
    FnDecl(
        IdnDef("uncurried"),
        Vector(
            ParamDecl(IdnDef("x"), IntType()),
            ParamDecl(IdnDef("y"), BoolType())),
        Some(IntType()),
        Block(
            Vector(
                IfExp(
                    IdnExp(IdnUse("y")),
                    Block(Vector(IdnExp(IdnUse("x")))),
                    Block(
                        Vector(StarExp(IdnExp(IdnUse("x")), IntExp(2))))))))
  }
}

test ("testing simple appExp") {
  expression("f(a-3)") should parseTo[Expression] {

        AppExp(IdnExp(IdnUse("f")), Vector(MinusExp(IdnExp(IdnUse("a")), IntExp(3))))
  }
}

test("testing complex appExp"){
  expression("f(4)(4)(a)") should parseTo[Expression] {
    AppExp(
            AppExp(
                AppExp(IdnExp(IdnUse("f")), Vector(IntExp(4))),
                Vector(IntExp(4))),
            Vector(IdnExp(IdnUse("a"))))
  }
}





test ("factorial function program") {
  program ("fn fact(n: int) -> int {if n = 0 { 1 } else { n * fact(n-1) }};fact(5)") should parseTo[Program]{
    Program(
        Vector(
            FnDecl(
                IdnDef("fact"),
                Vector(ParamDecl(IdnDef("n"), IntType())),
                Some(IntType()),
                Block(
                    Vector(
                        IfExp(
                            EqualExp(IdnExp(IdnUse("n")), IntExp(0)),
                            Block(Vector(IntExp(1))),
                            Block(
                                Vector(
                                    StarExp(
                                        IdnExp(IdnUse("n")),
                                        AppExp(
                                            IdnExp(IdnUse("fact")),
                                            Vector(
                                                MinusExp(
                                                    IdnExp(IdnUse("n")),
                                                    IntExp(1))))))))))),
            AppExp(IdnExp(IdnUse("fact")), Vector(IntExp(5)))))
  }
}

test ("testing fibonacci function program") {

  program("fn fibonacci(n : int) -> int {if n < 0 { return -1 } else {}; let mut res1 = 0;let mut res2 = 1;  let mut count = n;while 0 < count {let temp = res1;res1 := res2;res2 := temp + res1;count := count - 1};res1};  fibonacci(5)") should parseTo[Program]{

    Program(
        Vector(
            FnDecl(
                IdnDef("fibonacci"),
                Vector(ParamDecl(IdnDef("n"), IntType())),
                Some(IntType()),
                Block(
                    Vector(
                        IfExp(
                            LessExp(IdnExp(IdnUse("n")), IntExp(0)),
                            Block(Vector(Return(Some(NegExp(IntExp(1)))))),
                            Block(Vector())),
                        LetMutDecl(IdnDef("res1"), IntExp(0)),
                        LetMutDecl(IdnDef("res2"), IntExp(1)),
                        LetMutDecl(IdnDef("count"), IdnExp(IdnUse("n"))),
                        WhileExp(
                            LessExp(IntExp(0), IdnExp(IdnUse("count"))),
                            Block(
                                Vector(
                                    LetDecl(
                                        IdnDef("temp"),
                                        IdnExp(IdnUse("res1"))),
                                    AssignExp(
                                        IdnUse("res1"),
                                        IdnExp(IdnUse("res2"))),
                                    AssignExp(
                                        IdnUse("res2"),
                                        PlusExp(
                                            IdnExp(IdnUse("temp")),
                                            IdnExp(IdnUse("res1")))),
                                    AssignExp(
                                        IdnUse("count"),
                                        MinusExp(
                                            IdnExp(IdnUse("count")),
                                            IntExp(1)))))),
                        IdnExp(IdnUse("res1"))))),
            AppExp(IdnExp(IdnUse("fibonacci")), Vector(IntExp(5)))))

  }

}

}
