/*
 * This file is part of COMP332 Assignment 2 2018.
 *
 * Lintilla, a simple functional programming language.
 *
 * © 2018, Dominic Verity and Anthony Sloane, Macquarie University.
 *         All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Parser for the Lintilla language.
 */

package lintilla

import org.bitbucket.inkytonik.kiama.parsing.Parsers
import org.bitbucket.inkytonik.kiama.util.Positions

/**
 * Module containing parsers for Lintilla.
 */
class SyntaxAnalysis (positions : Positions)
    extends Parsers (positions) {

  import LintillaTree._
  import scala.language.postfixOps

  // Top level parser.
  lazy val parser : PackratParser[Program] =
    phrase(program)

  // Parses a whole Lintilla program.
  lazy val program : PackratParser[Program] =

    repsep(expression , ";") ^^ Program



   lazy val expression : PackratParser [Expression] =
     factors|paramDecl|letDecl|whileExp|assignExp|block|fnDecl|ifExp|returnExp|appExp


    lazy val paramDecl : PackratParser[ParamDecl] =
     (idndef <~ ":") ~ tipe ^^ ParamDecl

    lazy val assignExp : PackratParser[AssignExp] =
      (idnuse <~ ":=" ) ~ expression ^^ AssignExp

       lazy val block : PackratParser [Block] =
         "{" ~>repsep(expression , ";")  <~ "}" ^^ Block

   lazy val ifExp : PackratParser [IfExp] =
   ("if" ~> expression ) ~ block ~ ("else" ~> block) ^^ IfExp

    lazy val whileExp : PackratParser [WhileExp] =
      ("while" ~> expression) ~ block ^^ WhileExp

   lazy val returnExp : PackratParser [Return] =
     "return" ~> opt(expression) ^^ {case Some(tp) => Return(Option(tp))
                                    case None  => Return(None)}

  lazy val letDecl : PackratParser [Expression] =
    ("let" ~> opt("mut")) ~ (idndef <~ "=") ~ expression ^^ {
        case None ~ n ~ e => LetDecl(n,e) // code to build a `LetDecl` node.
        case Some(_) ~ n ~ e => LetMutDecl (n,e)// code to build a `LetMutDecl` node.
    }


   lazy val  fnDecl : PackratParser [FnDecl] =
   "fn" ~> idndef  ~ ("(" ~> (repsep(paramDecl , ",") ) <~ ")") ~ opt("->" ~> tipe) ~ block ^^ FnDecl

 lazy val appExp : PackratParser [Expression] =
     appExp ~ ("(" ~> repsep(expression, ",") <~ ")") ^^ {case a ~ b => AppExp(a,b)
     }|idnuse ^^ IdnExp







  lazy val tipe : PackratParser[Type] =
    "unit" ^^^ UnitType() |
    "bool" ^^^ BoolType() |
    "int" ^^^ IntType()|
    "fn" ~> ("(" ~> repsep(tipe,",") <~ ")") ~ opt("->" ~> tipe) ^^ {case tps ~ Some(tp) => FnType(tps, tp)
                                                                     case tps ~ None     => FnType(tps, UnitType())}|
    "(" ~> tipe <~")"







  lazy val factors : PackratParser [Expression] =
    factors ~ ("=" ~> factors1) ^^ EqualExp |
    factors ~ ("<" ~> factors1) ^^ {case e ~ t => LessExp (e,t) } |
    idnuse ~ (":=" ~> factors1) ^^ {case e ~ t => AssignExp(e,t)}|
    //idnuse ~ (":=" ~> idnuse) ^^  {case e ~ t => AssignExp(e,t)}|
    factors1


  lazy val factors1 : PackratParser [Expression] =
    factors1 ~ ("+" ~> factors2) ^^ {case e ~ t => PlusExp (e,t) } |
    factors1 ~ ("-" ~> factors2) ^^ {case e ~ t => MinusExp (e,t) } |
    factors2

  lazy val factors2 : PackratParser[Expression] =
    "true" ^^^ BoolExp(true) |
    "false" ^^^ BoolExp(false) |
    factors2 ~ ("*" ~> factors2) ^^ {case e ~ t => StarExp (e,t)}|
    factors2 ~ ("/" ~> factors2) ^^ {case e ~ t => SlashExp (e,t)}|
    integer ^^ (s => IntExp (s.toInt))|
    //idnuse ^^ ( s => IdnExp (s))|
    "(" ~> expression <~ ")"|
    "-" ~> factors2 ^^ {case e => NegExp(e)}|
    block|
    appExp
    //identifier ^^ (s => IdnExp (Identifier(s)) )








  // Parses a literal integer.
  lazy val integer : PackratParser[String] =
    regex("[0-9]+".r)

  // Parses a defining occurence of an identifier.
  lazy val idndef : PackratParser[IdnDef] =
    identifier ^^ IdnDef

  // Parses an applied opccurence of an identifier.
  lazy val idnuse : PackratParser[IdnUse] =
    identifier ^^ IdnUse

  // Parses a legal identifier. Checks to ensure that the word parsed is
  // not a Lintilla keyword.
  lazy val identifier : PackratParser[String] =
    (not(keyword) | failure("identifier expected but keyword found")) ~>
      "[a-zA-Z][a-zA-Z0-9_]*".r

  // Parses any legal Lintilla keyword. This parser ensures that the keyword found
  // is not a prefix of an longer identifier. So this parser will not parse the
  // "int" prefix of "integer" as a keyword.
  lazy val keyword =
    keywords("[^a-zA-Z0-9_]".r,
             List("bool", "else", "false", "fn", "if", "int", "let", "mut",
                  "return", "true", "unit", "while")) |
      failure("expecting keyword")

  // We use the character class `\R` here to match line endings, so that we correctly
  // handle all of the end-line variants in un*x, MacOSX, MS Windows, and unicode.
  override val whitespace: Parser[String] =
    """(\s|(//.*(\R|\z)))*""".r
}
