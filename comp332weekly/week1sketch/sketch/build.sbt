name := "Sketch"

version := "1.0"

scalaVersion := "2.12.2"

scalacOptions ++= Seq ("-deprecation", "-feature", "-unchecked")

logLevel := Level.Info

fork := true

libraryDependencies ++=
    Seq (
        "junit" % "junit" % "4.12" % "test",
        "org.scalatest" %% "scalatest" % "3.0.3" % "test"
    )
