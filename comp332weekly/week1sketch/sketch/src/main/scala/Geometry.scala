/**
 * General facilities for geometrical operations.
 */
object Geometry {

  import scala.math.{abs, sqrt}

  /**
   * Return the distance beween points `pt1` and `pt2`.
   */
  def distance (pt1 : (Int, Int),
                pt2 : (Int, Int)) : Double = {
    val (x1, y1) = pt1
    val (x2, y2) = pt2
    val xdist = abs (x1 - x2)
    val ydist = abs (y1 - y2)
    sqrt (sqr (xdist) + sqr (ydist))
  }

  /**
   * Return a printable version of point `pt`.
   */
  def pointToString (pt : (Int, Int)) : String =
    s"(${pt._1},${pt._2})"

  /**
   * Return the square of `i`.
   */
  def sqr (i : Int) : Int = i * i



}
