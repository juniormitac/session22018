import Geometry.{distance, sqr}
import processing.core.PApplet
import processing.core.PConstants.CORNERS
import scala.math.abs

/**
 * Interface for drawwable shapes.
 */
abstract class Shape {
  def area : Double
  def contains (p : (Int, Int)) : Boolean
  def draw (applet : PApplet)
}

/**
 * Interface for builders of shapes.
 */
abstract class ShapeBuilder {
  def build (p1 : (Int, Int), p2 : (Int, Int), fillColour : Int) : Shape
  def description : String
}

/**
 * Rectangles defined by two opposite corners.
 */
class Rectangle (corner1 : (Int, Int),
                 corner2 : (Int, Int),
                 fillColour : Int = 255) extends Shape {

  val corner1X = corner1._1
  val corner1Y = corner1._2
  val corner2X = corner2._1
  val corner2Y = corner2._2

  lazy val area : Double =
    abs (corner1X - corner2X) *
      abs (corner1Y - corner2Y)

  lazy val minX = corner1X min corner2X
  lazy val minY = corner1Y min corner2Y
  lazy val maxX = corner1X max corner2X
  lazy val maxY = corner1Y max corner2Y

  def contains (pt : (Int, Int)) : Boolean =
    (minX <= pt._1) && (pt._1 <= maxX) &&
       (minY <= pt._2) && (pt._2 <= maxY)

  def draw (applet : PApplet) {
    applet.fill (fillColour)
    applet.rectMode (CORNERS)
    applet.rect (corner1X, corner1Y,
                 corner2X, corner2Y)
  }

}

/**
 * Rectangle builder.
 */
object Rectangle extends ShapeBuilder {

  def build (p1 : (Int, Int),
             p2 : (Int, Int),
             fillColour : Int) : Rectangle =
    new Rectangle (p1, p2, fillColour)

  val description = "Rectangle"

}

/**
 * Circles define by a `center` point and a `diameter`.
 */
class Circle (center : (Int, Int), diameter : Int,
              fillColour : Int = 255) extends Shape {

  val centerX = center._1
  val centerY = center._2

  lazy val radius = diameter / 2

  lazy val area : Double =
    scala.math.Pi * sqr (radius)

  def contains (pt : (Int, Int)) : Boolean =
    distance (center, pt) <= radius

  def draw (applet : PApplet) {
    applet.fill (fillColour)
    applet.ellipse (centerX, centerY, diameter, diameter)
  }

}

/**
 * Circle builder.
 */
object Circle extends ShapeBuilder {

  def build (p1 : (Int, Int), p2 : (Int, Int), fillColour : Int) : Circle = {
    val diameter = 2 * distance (p1, p2)
    new Circle (p1, diameter.toInt, fillColour)
  }

  val description = "Circle"

}

class Triangle (corner1 : (Int, Int), corner2 : (Int, Int), fillColour : Int = 255) extends Shape{

  val corner1X = corner1._1
  val corner1Y = corner1._2

  val corner2X = corner2._1
  val corner2Y = corner2._2

  lazy val area = (abs (corner1X - corner2X) *
    abs (corner1Y - corner2Y))/2

    lazy val minX = corner1X min corner2X
    lazy val minY = corner1Y min corner2Y
    lazy val maxX = corner1X max corner2X
    lazy val maxY = corner1Y max corner2Y

  def draw (applet : PApplet){

    applet.fill(fillColour)
    applet.triangle(corner1X,corner1Y,corner1X,corner2Y,corner2X,corner1Y)
  }

  def contains (pt : (Int, Int)) : Boolean =
    (minX <= pt._1) && (pt._1 <= maxX) &&
       (minY <= pt._2) && (pt._2 <= maxY)


}

object Triangle extends ShapeBuilder {

  def build (p1 : (Int, Int), p2: (Int, Int), fillColour : Int) : Triangle =
    new Triangle (p1, p2, fillColour)

  val description = "Triangle"
}
