import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class GeometryTests extends FunSuite {

  import Geometry._

  test ("The sqr method should square zero to get zero") {
      assertResult (0) (sqr (0))
  }

  test ("The sqr method should square five to get twenty-five") {
      assertResult (25) (sqr (5))
  }

  test ("The sqr method should square minus two to get four") {
      assertResult (4) (sqr (-2))
  }

  test ("The distance method should say point to itself is zero") {
      assertResult (0.0) (distance ((1, 1), (1, 1)))
  }

  test ("The distance method should say origin to y-axis point is y coord") {
      assertResult (10.0) (distance ((0, 0), (0, 10)))
  }

  test ("The distance method should say origin to x-axis point is x coord") {
      assertResult (20.0) (distance ((0, 0), (20, 0)))
  }

  test ("The distance method should say separation of NW-SE points is correct") {
      assertResult (5.385164807134504) (distance ((3, 5), (5, 10)))
  }

  test ("The distance method should say separation of SE-NW points is correct") {
      assertResult (5.385164807134504) (distance ((5, 10), (3, 5)))
  }

  test ("The distance method should say separation of SW-NE points is correct") {
      assertResult (5.385164807134504) (distance ((5, 5), (3, 10)))
  }

  test ("The distance method should say separation of NE-SW points is correct") {
      assertResult (5.385164807134504) (distance ((3, 10), (5, 5)))
  }

}
