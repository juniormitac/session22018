/* Template for class BigIntExtended.
 * This class is intended to extend the functionality provided 
 * by class BigInt.
 * This template is provided as part of Assignment 2 for COMP333.
 * Every JUnit test routine used in marking each method you write
 * will conform to the stated precondition and description of
 * output for the method.
 */
 package comp333;

import java.math.BigInteger;

public class BigIntExtended {
	
	// Euclid's algorithm, extended form.
	// Pre: a and b are positive big integers.
	// Returns: triple (d, x, y) such that
	//          d = gcd(a,b) = ax + by.
	
	//Modified to work with integer only
	
	public static Integer[] egcd(int a, int b) {
	
		Integer[] result = new Integer[3];
		
		int x = 0;
		int y = 1;
		
		int x1 = 1;
		int y1 = 0;
		
		//keeps values of a and b since they are changed in operation below
		int a1 = a;
		int b1 = b;
		
		int temp;
		
		while ( b != 0) {
			
			int q = a/b;
			int r = a % b;
			
			a = b;
			b = r;
			
			temp = x ;
			x = x1 - q * x;
			x1 = temp;
			
			temp = y;
			y = y1 - q * y;
			y1 = temp;
			
		}
		
		int gcd = (a1 * x1) + (b1 * y1);
		
		result[0]= gcd;
		result[1]= x1;
		result[2] = y1;
		
		
		return result;
	}
	
	// Modular inversion.
	// Pre: a and n are positive big integers which are coprime.
	// Returns: the inverse of a modulo n.
	//modified to work with int
	//returns -1 if a and n are not coprime
	public static Integer minv(Integer a, Integer n) {
		
		Integer result = 0;
		
		Integer[] prime = egcd (a,n);
		
		//return -1 if greatest common denom is > 1 since numbers are then not coprime
		if (prime[0] != 1) {
			return -1;
		}
		
		else {
			result = ((prime[1] % n) + n ) %n ;
		}
				
		return result;
	}
	
	// Chinese remainder algorithm.
	// Pre: p and q are different big prime numbers.
	//      0 <= a < p and 0 <= b < q.
	// Returns: the unique big int c such that
	//          c is congruent to a modulo p,
	//          c is congruent to b modulo q,
	//          and 0 <= c < pq.
	public static Integer cra(Integer p, Integer q, Integer a, Integer b) {
		Integer c ;
		
		Integer[] primes = egcd(p,q);
		
		
		//returns -1 if p q are not coprime
		if (primes[0] != 1) {
			
			return -1;
			
		}
		
		else {
			c = (a * primes[2] * q) + ( b * primes[1] * p);
		}
			
		
		return c;
	}
	
	// Modular exponentiation.
	// Pre: a and b are nonnegative big integers.
	//      n is a positive big integer.
	// Returns: this method returns a^b mod n.
	public static BigInt modexp(BigInt a, BigInt b, BigInt n) {
		
		BigInt c = new BigInt(0);
		
		BigInt d = new BigInt(1);
				
		while (b.isEqual(new BigInt(0)) == false) {
			
			BigInt[] mod;
			
			c = c.multiply(new BigInt(2));
			
			mod =(d.multiply(d)).divide(n);				

			d = mod [1];
			
			
			
			
		}
		
		
	
		// Complete this code.
		
		return d;
	}
	
	// Modular exponentiation, version 2.
	// This method returns a^b mod n, in case n is prime.
	public static BigInt modexpv2(BigInt a, BigInt b, BigInt n) {
        BigInt result = new BigInt();
		
		// Complete this code.
		
		return result;
	}
	
	// Modular exponentiation, version 3.
	// This method returns a^b mod n, in case n = pq, for different
	// primes p and q.
		public static BigInt modexpv3(BigInt a, BigInt b, BigInt n) {
	        BigInt result = new BigInt();
			
			// Complete this code.
			
			return result;
	}
		
	// Pre: a and b are nonnegative big integers of equal length.
	// Returns: the product of a and b using karatsuba's algorithm.
	public static BigInt karatsuba(BigInt a, BigInt b) {
		BigInt result = new BigInt();
		
		// Complete this code.
		
		return result;
	}
	
	// Pre: n is an odd big integer greater than 4.
	//      s is an ordinary positive integer.
	// Returns: if n is prime then returns true with certainty,
	//          otherwise returns false with probability
	//          1 - 4^{-s}.
	public static boolean millerrabin(BigInt n, int s) {
	
		// Complete this code.
		
		return true;
	}
	
	// Pre: l and s are ordinary positive integers.
	// Returns: a random "probable" big prime number n of length l decimal digits.
	//          The probability that n is not prime is less than 4^{-s}.
	public static BigInt randomprime(int l, int s) {
		BigInt result = new BigInt();
		
		// Complete this code.
		
		return result;
	}

	public static void main(String[] args) {
		
		Integer[] a = egcd (10 , 3);
		
		Integer b = cra(3 , 5 , 2, 3);
		
		BigInteger c = new BigInteger("1");
		
		
	
	
		System.out.println(a[0] + " " + a[1] + " " + a[2]);
		System.out.println(b);
		
		// Implement a simple interactive RSA demo program here.

	}
	
	
}
